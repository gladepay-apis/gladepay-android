package com.gladepay.android;

import android.app.Activity;
import android.util.Log;

import com.gladepay.android.client.ApiClient;
import com.gladepay.android.service.GladepayApiService;
import com.google.gson.JsonParser;


import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class Gladepay {

    private static final String LOG_TAG = Gladepay.class.getSimpleName();
    /**
     * merchantId
     */
    private String merchantId;

    /**
     * merchantKey
     */
    private String merchantKey;

    /**
     * isLive boolean status if live mode or demo mode
     */
    private boolean isLive;



    private GladepayApiService gladepayApiService;


    JsonParser jsonParser = new JsonParser();

    PaymentTransactionManager paymentTransactionManager;

    PTransaction transaction;
    TransactionCallback transactionCallback;



    /**
     * This initializes the Merchant Id and the merchant Key as well as sets the mode
     * If LIVE or DEMO
     * @param merchantId
     * @param merchantKey
     * @param isLive    true - LIVE
     *                  false - DEMO
     *
     */
    public Gladepay(String merchantId, String merchantKey, boolean isLive)
    {
        this.merchantId = merchantId;
        this.merchantKey = merchantKey;
        this.isLive = isLive;

        try {

            gladepayApiService = new ApiClient(this.merchantId, this.merchantKey, this.isLive).getGladepayApiService();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }


    public void chargeCard(Activity activity, Charge charge, TransactionCallback transactionCallback) {
        chargeThisCard(activity, charge, transactionCallback);
    }


    private void chargeThisCard(Activity activity, Charge charge, TransactionCallback transactionCallback) {
        try {

            Log.i("GLADEPAY_CLASS: ", "CHARGE-THIS-CARD");
            PaymentTransactionManager transactionManager = new PaymentTransactionManager(activity, charge, transactionCallback);
            transactionManager.chargeCard();

        } catch (Exception ae) {
            assert transactionCallback != null;
            transactionCallback.onError(ae, null);
        }
    }


    private interface BaseCallback {
    }

    public interface TransactionCallback extends BaseCallback {
        void onSuccess(PTransaction transaction);
        void beforeValidate(PTransaction transaction);

        void onError(Throwable error, PTransaction transaction);
    }


}