package com.gladepay.android;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.gladepay.android.apiui.Auth3DActivity;
import com.gladepay.android.apiui.OtpPinActivity;
import com.gladepay.android.apiui.AuthSingleton;
import com.gladepay.android.client.ApiClient;
import com.gladepay.android.exceptions.ProcessingException;
import com.gladepay.android.request.CardChargeRequestBody;
import com.gladepay.android.service.GladepayApiService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentTransactionManager {

    private static final String LOG_TAG = PaymentTransactionManager.class.getSimpleName();
    private static boolean PROCESSING = false;
    private final Charge charge;
    private final Activity activity;
    private final PTransaction paymentTransaction;
    private final Gladepay.TransactionCallback transactionCallback;
    private GladepayApiService apiService;
    private final AuthSingleton osi = AuthSingleton.getInstance();
    private String validateMessage; // message to show during OTP validation

    private final Callback<JsonObject> serverCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            handleGladepayApiResponse(response);
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.e(LOG_TAG, t.getMessage());
            notifyProcessingError(t);
        }
    };
    private CardChargeRequestBody chargeRequestBody;


    void chargeCard() {
        try {
            if (charge.getCard() != null && charge.getCard().isValid()) {
                initiateTransaction();
                sendChargeRequestToServer();

            }else{
                Toast.makeText(this.activity.getApplicationContext(), "Please Enter Valid Card Details", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ce) {
            Log.e(LOG_TAG, ce.getMessage(), ce);
            if (!(ce instanceof ProcessingException)) {
                turnOffProcessing();
            }
            transactionCallback.onError(ce, paymentTransaction);
        }
    }


    public PaymentTransactionManager(Activity activity, Charge charge, Gladepay.TransactionCallback transactionCallback) {

        if (BuildConfig.DEBUG && (activity == null)) {
            throw new AssertionError("please activity cannot be null");
        }
        if (BuildConfig.DEBUG && (charge == null)) {
            throw new AssertionError("charge must not be null");
        }
        if (BuildConfig.DEBUG && (charge.getCard() == null)) {
            throw new AssertionError("please add a card to the charge before calling chargeCard");
        }
        if (BuildConfig.DEBUG && (transactionCallback == null)) {
            throw new AssertionError("transactionCallback must not be null");
        }

        this.activity = activity;
        this.charge = charge;
        this.transactionCallback = transactionCallback;
        this.paymentTransaction = new PTransaction();

    }

    private void sendChargeRequestToServer() {
        try {
            initiateChargeOnServer();
        }catch (Exception ce) {
            Log.e(LOG_TAG, ce.getMessage(), ce);
            notifyProcessingError(ce);
        }
    }

    private void initiateTransaction() throws ProcessingException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (PaymentTransactionManager.PROCESSING) {
            throw new ProcessingException();
        }
        turnOnProcessing();
        apiService = new ApiClient( GladepaySdk.getMerchantId(), GladepaySdk.getMerchantKey(), GladepaySdk.getLiveStatus()).getGladepayApiService();
        chargeRequestBody = new CardChargeRequestBody(charge);
        tLog("INIt transaction");
    }

    private void turnOnProcessing() {
        PaymentTransactionManager.PROCESSING = true;
    }

    private void turnOffProcessing() {
        PaymentTransactionManager.PROCESSING = false;
    }

    private void notifyProcessingError(Throwable t) {
        tLog("Error "+t.getMessage());
        turnOffProcessing();
        transactionCallback.onError(t, paymentTransaction);
    }

    private void initiateChargeOnServer(){
       tLog("InitiateChargeOnServer ChargeRequestServer");
        Call<JsonObject> call = apiService.initiateTransactions(chargeRequestBody.getInitiateParamsJsonObjects());
        call.enqueue(serverCallback);
    }

    private void validateTransactionServer(String otp){
       tLog("validateTransactionServer ValidateRequestServer");
        Log.v("PTM Charge Body",chargeRequestBody.getParamsJsonObjects("validate", paymentTransaction.getTransactionRef(), otp).toString() );
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("validate", paymentTransaction.getTransactionRef(), otp ));
        call.enqueue(serverCallback);
    }
    private void validateTransactionServer3d(String message){
        tLog("validateTransactionServer ValidateRequestServer");
        Log.v("PTM Charge Body",chargeRequestBody.getParamsJsonObjects("verify", paymentTransaction.getTransactionRef(), message).toString() );
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("verify", paymentTransaction.getTransactionRef(), message));
        call.enqueue(serverCallback);
    }

    private void validateTransactionServerPin(String pin){
        tLog("validateTransactionServer ValidateRequestServer");
        Log.v("PTM Charge Body",chargeRequestBody.getParamsJsonObjects("charge", paymentTransaction.getTransactionRef(), pin).toString() );
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("charge", paymentTransaction.getTransactionRef(), pin));
        call.enqueue(serverCallback);
    }

    private void requeryAndverifyTransactionServer(){
       tLog("InitiateChargeOnServer requeryAndVerifyTransactionServer");
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("verify", paymentTransaction.getTransactionRef(), null ));
        call.enqueue(serverCallback);
        requeryCount++;
    }

    private int requeryCount = 0;

    private void handleGladepayApiResponse(Response<JsonObject> response) {
        tLog("handleGladepayApiResponse");

        this.paymentTransaction.loadFromResponse(response.body());
        this.paymentTransaction.getStatusId();
        this.paymentTransaction.getTransactionRef();

        JsonObject responseJsonObjectBody;

        if(response == null){
            responseJsonObjectBody = new Gson().fromJson("{\"status\": \"error\"}", JsonObject.class);
        }else{
            responseJsonObjectBody = response.body();
        }

        if(this.paymentTransaction.hasStartedProcessingOnServer()){
            Log.i(getClass().getName()+" ON-RESPONSE: ",  ( new Gson().toJson(response.body())) );
            Log.i(getClass().getName()+" ON-RESPONSE STAT: ",  (this.paymentTransaction.getStatusId() ));
            Log.i(getClass().getName()+" ON-RESPONSE Message:",  ( response.message().toString()) );
            Log.i(getClass().getName()+" ON-RESPONSE E-BDY:",  "[ " + ( response.isSuccessful() +" ]") );
            Log.i(getClass().getName()+" ON-RESPONSE E-BDY:",  ( response.raw().body().toString()) );
        }

//        }else{
//            turnOffProcessing();
//            transactionCallback.onError(new Throwable("Server Processing On Server"), paymentTransaction);
//        }

        String outcome_status = responseJsonObjectBody.get("status").getAsString().toLowerCase();

        switch (outcome_status){
            case "error":
                Log.i("RESPONSE-HR: ", "STATUS:    " + "ERROR");
                break;
            case "success":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "SUCCESS");
                break;
            case "104":
                String message = responseJsonObjectBody.get("message").getAsString();
                Log.v("PTM", "104: " + message);
                if(message.length() >0 ) {
                    paymentTransaction.setMessage(message);
                    Log.i("RESPONSE-HR: ", "STATUS:    " + "104");
                    turnOffProcessing();
                    transactionCallback.onError(new Throwable(message), paymentTransaction);
                    return;
                }
                break;
            case "202":
                String apply_auth;

                if(responseJsonObjectBody.has("apply_auth")){
                    apply_auth = responseJsonObjectBody.get("apply_auth").getAsString();
                    if(apply_auth.equalsIgnoreCase("otp")){
                        new OtpAsyncTask().execute("otp");
                        validateMessage = responseJsonObjectBody.get("validate").getAsString();
                        Log.v("PTM", "OTP Message: " + validateMessage);
                        return;
                    } else if (apply_auth.equalsIgnoreCase("pin")){
                        new PinAsyncTask().execute("pin");
                    }
                }
                else if (responseJsonObjectBody.has("auth_type")) {
                    if (responseJsonObjectBody.get("auth_type").getAsString().equalsIgnoreCase("otp")){
                        validateMessage = responseJsonObjectBody.get("validate").getAsString();
                        new OtpAsyncTask().execute("otp");
                    }

                } else if (responseJsonObjectBody.has("authURL")){
                    String url = responseJsonObjectBody.get("authURL").toString();
                    Log.v("PTM", "REsponse: " + url);
                    new _3DAsyncTask().execute(url);
//                    new _3D(url);
//                    Intent intent = new Intent(activity, _3D.class);
//                    activity.startActivity(intent);
//                    activity.startActivity(new Intent(activity, _3D.class));
                }else{
                    //default to using OTP
                    new OtpAsyncTask().execute("otp");
                    validateMessage = responseJsonObjectBody.get("validate").getAsString();
                    Log.v("PTM", "OTP Message: " + validateMessage);
                    return;
                }
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "202");
                break;
            case "200":
                Log.v("PTM", "payment transaction: " + paymentTransaction.hasStartedProcessingOnServer());

                String txnStatus = null;
                String successfulStatus = "successful";

                if(responseJsonObjectBody.has("txnStatus")){
                    txnStatus = responseJsonObjectBody.get("txnStatus").getAsString();
                }

                if(paymentTransaction.hasStartedProcessingOnServer()) {
                    Log.v("PTM", "requery: " + requeryCount);

                    if (requeryCount >= 1) {
                        if(successfulStatus.equals(txnStatus)){
                            turnOffProcessing();
                            transactionCallback.onSuccess(paymentTransaction);
                            Log.i("RESPONSE-HR: ", "STATUS:    " + "200");
                            Log.i("RESPONSE-HR: ", "REQUERY-COUNT: > Number of Verification Exceeded");
//                        requeryCount = 0;

                            Log.v("TXN_STATUS_1", "TXN_STATUS_1: " + txnStatus);
                            return;
                        }

                    }

                    if (paymentTransaction.getStatusId().equals("200")){

                            Log.v("PTM", "Called");
                            Log.v("TXN_STATUS", "TXN_STATUS: " + txnStatus);
                            turnOffProcessing();
                            transactionCallback.onSuccess(paymentTransaction);


                    }
                }

                if(responseJsonObjectBody.has("cardToken") && responseJsonObjectBody.has("message") && responseJsonObjectBody.get("bank_message") == null){
                    // currently using for OTP
                    String cardToken = responseJsonObjectBody.get("cardToken").getAsString();
                    message = responseJsonObjectBody.get("message").getAsString();


                    paymentTransaction.setMessage(message);
                    if(message.toLowerCase().contains("success") || message.toLowerCase().contains("approved")){ // message = Approved by Financial Institution
                        Log.i("RESPONSE-HR: ", "ABOUT-REQUERY&VERIFY -MESSAGE");
                        new CountDownTimer(2000, 2000) {
                            public void onFinish() {
                                requeryAndverifyTransactionServer();
                            }

                            public void onTick(long millisUntilFinished) {
                            }
                        }.start();
                        return;
                    }
                }else if(responseJsonObjectBody.has("cardToken") && responseJsonObjectBody.has("bank_message")){
                    // for 3D Auth
                    String cardToken = responseJsonObjectBody.get("cardToken").getAsString();
                    message = responseJsonObjectBody.get("bank_message").getAsString();
                    paymentTransaction.setMessage(message);
                    if(message.toLowerCase().contains("approved")){
                        Log.i("RESPONSE-HR: ", "ABOUT-REQUERY&VERIFY -BANK_MESSAGE");
                        new CountDownTimer(2000, 2000) {
                            public void onFinish() {
                                requeryAndverifyTransactionServer();
                            }

                            public void onTick(long millisUntilFinished) {
                            }
                        }.start();
                        return;
                    }
                }

                break;
            case "500":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "500");
                break;
            case "400":
                String message400 = responseJsonObjectBody.get("message").getAsString();
                transactionCallback.onError(new Throwable(message400), paymentTransaction);
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "400");
//                break;
            case "401":
                Log.i("RESPONSE-HR: ", "STATUS:    "   + "401");
                break;
            case "402":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "402");
                break;
            case "403":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "403");
                break;
            case "300":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "300");
                break;
            case "301":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "301");
                break;
            default:
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "DEFAULT" +response.body().get("status").getAsString());

        }

        Log.i("RESPONSE-HR: ", new Gson().toJson(responseJsonObjectBody.toString()));

        if (outcome_status.equalsIgnoreCase("202") || outcome_status.equalsIgnoreCase("success")) {
            turnOffProcessing();
            transactionCallback.onSuccess(this.paymentTransaction);
            return;
        }

    }

    private void tLog(String tag){
        Log.i(getClass().getSimpleName(), tag);
    }

    private class OtpAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Intent i = new Intent(activity, OtpPinActivity.class);
            i.putExtra("auth_type", params[0]);
            i.putExtra("message", validateMessage);
            activity.startActivity(i);
            turnOnProcessing();

            synchronized (osi) {
                try {
                    osi.wait();
                } catch (InterruptedException e) {
                    notifyProcessingError(new Exception("OTP entry Interrupted"));
                }
            }

            return osi.getOtp();
        }

        @Override
        protected void onPostExecute(String otp) {
            super.onPostExecute(otp);
            turnOffProcessing();
            if (otp != null) {
                PaymentTransactionManager.this.validateTransactionServer(otp); //.validate();
            } else {
                notifyProcessingError(new Exception("You did not provide an OTP"));
            }
        }
    }

    private class PinAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Intent i = new Intent(activity, OtpPinActivity.class);
            i.putExtra("auth_type", params[0]);
            activity.startActivity(i);
            turnOnProcessing();

            synchronized (osi) {
                try {
                    osi.wait();
                } catch (InterruptedException e) {
                    notifyProcessingError(new Exception("Pin entry Interrupted"));
                }
            }

            return osi.getPin();
        }

        @Override
        protected void onPostExecute(String pin) {
            super.onPostExecute(pin);
//            turnOffProcessing();
            turnOnProcessing();
            if (pin != null) {
                PaymentTransactionManager.this.validateTransactionServerPin(pin); //.validate();
            } else {
                notifyProcessingError(new Exception("You did not provide a PIN"));
            }
        }
    }

    private class _3DAsyncTask extends AsyncTask<String, Void, String> {
        Intent i = new Intent(activity, Auth3DActivity.class);
        @Override
        protected String doInBackground(String... strings) {
            i.putExtra("url", strings[0]);
            activity.startActivity(i);
            turnOnProcessing();

            synchronized (osi){
                try {
                    osi.wait();
                } catch (InterruptedException e){
                    notifyProcessingError(new Exception("3D auth Interrupted"));
                }
            }

            return osi.getAuthMessage();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.v("PTM", "Post exec: " + s);
            if (s.contains("APPROVED")){
                PaymentTransactionManager.this.validateTransactionServer3d(s);
            } else {
                notifyProcessingError(new Exception("Transaction Declined"));
            }
        }
    }
}
