package com.gladepay.android.exceptions;

/**
 * @author Light Chinaka.
 */
public class ProcessingException extends ChargeException {
    public ProcessingException() {
        super("A transaction is currently being processed, please wait till it is completed before attempting a new charge.");
    }
}
