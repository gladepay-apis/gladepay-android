package com.gladepay.androidexample;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.gladepay.android.Card;
import com.gladepay.android.Charge;
import com.gladepay.android.Gladepay;
import com.gladepay.android.GladepaySdk;
import com.gladepay.android.PTransaction;
import com.gladepay.android.exceptions.ExpiredAccessCodeException;

import org.json.JSONException;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String NAME_OF_CLASS = "MAIN-ACTIVITY";

    boolean isLiveServer = true;


    // Set this to a public key
    private String merchantId = "GP_AdOMrhalGYqxj1MPgH47PH4wZYsKhRlt";
    // Set the private key
    private String merchantKey = "2bm2da9rW1wCN2uU8ThmzGCtwRXBqfNXymJ";

    EditText mEditCardNum;
    EditText mEditCVC;
    EditText mEditExpiryMonth;
    EditText mEditExpiryYear;
    EditText mEditFullName;
    EditText mEditAmount;

    TextView mTextError;

    ProgressDialog dialog;
    private TextView mTextReference;
    private Charge charge;
    private PTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditCardNum = findViewById(R.id.edit_card_number);
        mEditCVC = findViewById(R.id.edit_cvc);
        mEditExpiryMonth = findViewById(R.id.edit_expiry_month);
        mEditExpiryYear = findViewById(R.id.edit_expiry_year);
        mEditFullName = findViewById(R.id.edit_fullname);
        mEditAmount = findViewById(R.id.edit_amount);

        Button mButtonPerformTransaction = findViewById(R.id.button_perform_transaction);
        Button generateCardButton = findViewById(R.id.button_random_card);

        mTextError = findViewById(R.id.textview_error);

        mTextReference = findViewById(R.id.textview_reference);

        //initialize sdk
        GladepaySdk.initialize(getApplicationContext());
        GladepaySdk.setLiveStatus(isLiveServer);
        GladepaySdk.setMerchantId(merchantId);
        GladepaySdk.setMerchantKey(merchantKey);

        if (mButtonPerformTransaction == null) {
            Log.i(getClass().getSimpleName(), "NULL MBUTTON PERFORMED TRANSACTION");
        } else {
            Log.i(getClass().getSimpleName(), "NOT-NULL MBUTTON PERFORMED TRANSACTION");
        }

        //add the Click Listener
        mButtonPerformTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if(mEditAmount.getText().toString().length() != 0){
                        startAFreshCharge();
                    } else{
                        mEditAmount.setError("Amount is required");
                    }
                } catch (Exception e) {
                    MainActivity.this.mTextError.setText(String.format("An error occurred while charging card: %s %s", e.getClass().getSimpleName(), e.getMessage()));

                }
            }
        });

        generateCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditCardNum.setText(getString(R.string.card_number));
                mEditCVC.setText(getString(R.string.card_cvc));

                Calendar calendar = Calendar.getInstance();
                int m = calendar.get(Calendar.MONTH) + 1;
                String month = m > 10 ? String.valueOf(m) : "0" + m; // appending 0 for months before october<10>
                int y = calendar.get(Calendar.YEAR);
                String year = Integer.toString(y).substring(2);
                mEditExpiryMonth.setText(month);
                mEditExpiryYear.setText(year);
                mEditFullName.setText(getString(R.string.card_fullname));
            }
        });


    }

    private void startAFreshCharge() {
        // initialize the charge
        charge = new Charge();
        charge.setCard(retrieveAndPopulateCardFromForm());


        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Performing transaction... please wait");
        dialog.show();

        int amount = Integer.parseInt(mEditAmount.getText().toString().trim());

//        charge.setAmount(20000);
        charge.setAmount(amount);
        charge.setEmail("support@gladepay.com");


        Log.v("MainActivity", String.format("Transaction details: card number: %s, cvc: %s, " +
                        "expiry: %d/%d, amount: %d, name: %s", charge.getCard().getNumber(), charge.getCard().getCvc(), charge.getCard().getExpiryMonth(),
                charge.getCard().getExpiryYear(), charge.getAmount(), charge.getCard().getName()));

        charge.setReference("ChargedFromAndroid_" + Calendar.getInstance().getTimeInMillis());
        try {
            charge.putCustomField("Charged From", "Android SDK");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        chargeCard();
    }

    private void dismissDialog() {
        if ((dialog != null) && dialog.isShowing()) {
            Log.v("MainActivity", "DialogDismissed");
            dialog.dismiss();
        }
    }


    private void chargeCard() {
        transaction = null;

        GladepaySdk.chargeCard(MainActivity.this, charge, new Gladepay.TransactionCallback() {
            // This is called only after transaction is successful
            @Override
            public void onSuccess(PTransaction transaction) {
                Log.v("Mainactivity", "onSuccess");
                MainActivity.this.transaction = transaction;
                mTextError.setText(" ");
                Toast.makeText(MainActivity.this, transaction.getTransactionRef(), Toast.LENGTH_LONG).show();
                updateTextViews();
                Log.v("MainActivity", "Transaction status ID: " + transaction.getStatusId());
                Log.v("MainActivity", "Transaction status message: " + transaction.getMessage());
                if (!transaction.getMessage().isEmpty()){
                    dismissDialog();
                }
//                new verifyOnServer().execute(transaction.getTransactionRef());
            }

            // This is called only before requesting OTP
            // Save reference so you may send to server if
            // error occurs with OTP
            // No need to dismiss dialog
            @Override
            public void beforeValidate(PTransaction transaction) {
                MainActivity.this.transaction = transaction;
                Toast.makeText(MainActivity.this, transaction.getTransactionRef(), Toast.LENGTH_LONG).show();
                updateTextViews();
            }

            @Override
            public void onError(Throwable error, PTransaction transaction) {
                // If an access code has expired, simply ask your server for a new one
                // and restart the charge instead of displaying error
                MainActivity.this.transaction = transaction;
                if (error instanceof ExpiredAccessCodeException) {
                    MainActivity.this.startAFreshCharge();
                    MainActivity.this.chargeCard();
                    return;
                }

                dismissDialog();

                if (transaction.getTransactionRef() != null) {
                    Toast.makeText(MainActivity.this, transaction.getTransactionRef() + " concluded with error: " + error.getMessage(), Toast.LENGTH_LONG).show();
                    mTextError.setText(String.format("%s  concluded with error: %s %s", transaction.getTransactionRef(), error.getClass().getSimpleName(), error.getMessage()));
                } else {
                    Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    mTextError.setText(String.format("Error: %s %s", error.getClass().getSimpleName(), error.getMessage()));
                }
                updateTextViews();
            }

        });
    }

    private void updateTextViews() {
        if (transaction.getTransactionRef() != null) {
            mTextReference.setText(String.format("Reference: %s", transaction.getTransactionRef() + " " + transaction.getMessage()));
        } else {
            mTextReference.setText("No transaction");
        }
    }

   /* private class verifyOnServer extends AsyncTask<String, Void, String> {
        private String reference;
        private String error;

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                MainActivity.this.mTextError.setText(String.format("Gateway response: %s", result));

            } else {
                MainActivity.this.mTextError.setText(String.format("There was a problem verifying %s on the backend: %s ", this.reference, error));
                dismissDialog();
            }
        }

        @Override
        protected String doInBackground(String... reference) {
            try {
                this.reference = reference[0];
                return null;
            } catch (Exception e) {
                error = e.getClass().getSimpleName() + ": " + e.getMessage();
            }
            return null;
        }
    }*/


    /**
     * Method to validate the form, and set errors on the edittexts.
     */
    private Card retrieveAndPopulateCardFromForm() {
        //validate fields
        Card card;

        String cardNum = mEditCardNum.getText().toString().trim();

        //build card object with ONLY the number, update the other fields later
        card = new Card.CardBuilder(cardNum, 0, 0, "").build();
        String cvc = mEditCVC.getText().toString().trim();
        //update the cvc field of the card
        card.setCvc(cvc);

        String fullname = mEditFullName.getText().toString().trim();
        card.setName(fullname);

        //validate expiry month;
        String sMonth = mEditExpiryMonth.getText().toString().trim();
        int month = 0;
        try {
            month = Integer.parseInt(sMonth);
        } catch (Exception ignored) {
        }

        card.setExpiryMonth(month);

        String sYear = mEditExpiryYear.getText().toString().trim();
        int year = 0;
        try {
            year = Integer.parseInt(sYear);
        } catch (Exception ignored) {
        }
        card.setExpiryYear(year);

        return card;
    }
}
